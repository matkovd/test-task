package com.example.daniil.testtask.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.daniil.testtask.models.Item;

import java.util.ArrayList;

/**
 * Created by Daniil on 13.12.2016.
 */

public class DbHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "items.sqlite";
    public static final String TABLE_NAME = "items";
    public static final String ITEMS_COLUMN_ID = "id";
    public static final String ITEMS_COLUMN_DESCRIPTION = "description";
    public static final String ITEMS_COLUMN_PRICE = "price";
    public static final String ITEMS_COLUMN_COUNT = "count";

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, 3);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table items " +
                        "(id integer primary key autoincrement, description text,price integer,count integer)"
        );
    }

    public boolean insertItem(Item item) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("description", item.getDescription());
        contentValues.put("price", item.getCount());
        contentValues.put("count", item.getPrice());
        db.insert(TABLE_NAME, null, contentValues);
        db.close();
        return true;
    }

    public boolean deleteItem(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, "id = ?", new String[]{Integer.toString(id)});
        db.close();
        return true;
    }

    public boolean updateItem(Item item) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("description", item.getDescription());
        contentValues.put("price", item.getCount());
        contentValues.put("count", item.getPrice());
        db.update(TABLE_NAME, contentValues, "id = ?", new String[]{Long.toString(item.getId())});
        db.close();
        return true;
    }

    public ArrayList<Item> getAllItems() {
        ArrayList<Item> data = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("Select * from items", null);
        String description;
        Integer id;
        Integer price;
        Integer count;
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                id = cursor.getInt(cursor.getColumnIndex(ITEMS_COLUMN_ID));
                description = cursor.getString(cursor.getColumnIndex(ITEMS_COLUMN_DESCRIPTION));
                price = cursor.getInt(cursor.getColumnIndex(ITEMS_COLUMN_PRICE));
                count = cursor.getInt(cursor.getColumnIndex(ITEMS_COLUMN_COUNT));

                data.add(new Item(id, description, price, count));
                cursor.moveToNext();
            }
        }
        cursor.close();
        return data;
    }

    public Item getItemById(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Item ret = null;
        Cursor cursor = db.rawQuery("Select * from items where id=" + Integer.toString(id), null);
        String description;
        Integer price;
        Integer count;
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                id = cursor.getInt(cursor.getColumnIndex(ITEMS_COLUMN_ID));
                description = cursor.getString(cursor.getColumnIndex(ITEMS_COLUMN_DESCRIPTION));
                price = cursor.getInt(cursor.getColumnIndex(ITEMS_COLUMN_PRICE));
                count = cursor.getInt(cursor.getColumnIndex(ITEMS_COLUMN_COUNT));

                ret = (new Item(id, description, price, count));
                cursor.moveToNext();
            }
        }
        cursor.close();
        db.close();
        return ret;
    }

    public int numberOfRows() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.close();
        return (int) DatabaseUtils.queryNumEntries(db, TABLE_NAME);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS items");
        onCreate(db);
    }
}
