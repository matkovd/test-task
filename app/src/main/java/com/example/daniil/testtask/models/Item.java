package com.example.daniil.testtask.models;


public class Item {
    private long id;
    private String description;
    private long price;
    private long count;

    public Item(long id, String description, long price, long count) {
        this.id = id;
        this.description = description;
        this.price = price;
        this.count = count;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public long getId() {
        return id;
    }
}
