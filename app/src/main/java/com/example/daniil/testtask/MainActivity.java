package com.example.daniil.testtask;


import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.daniil.testtask.fragments.EditFragment;
import com.example.daniil.testtask.fragments.RecyclerViewFragment;

public class MainActivity extends FragmentActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (findViewById(R.id.fragment_container_main) != null) {
            if (savedInstanceState != null) {
                return;
            }
            RecyclerViewFragment firstFragment = new RecyclerViewFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_container_main, firstFragment).commit();
        }
    }

    public void addNew(View view) {
        EditFragment editFragment = new EditFragment();
        if (findViewById(R.id.fragment_container_edit) != null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_edit, editFragment).commit();
        } else {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_main, editFragment).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public FragmentManager getMyFragmentManager() {
        return getSupportFragmentManager();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
