package com.example.daniil.testtask.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.example.daniil.testtask.R;
import com.example.daniil.testtask.db.DbHelper;
import com.example.daniil.testtask.models.Item;


public class EditFragment extends Fragment {
    public static void hideKeyboard(Context context) {
        InputMethodManager inputManager = (InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        View v = ((Activity) context).getCurrentFocus();
        if (v == null) {
            return;
        }
        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.edit, container, false);
        super.onCreate(savedInstanceState);
        final Bundle args = getArguments();
        if (args != null) {
            if (args.get("edit") != null) {
                if ((boolean) args.get("edit")) {
                    EditText description = (EditText) rootView.findViewById(R.id.description);
                    EditText count = (EditText) rootView.findViewById(R.id.count);
                    EditText price = (EditText) rootView.findViewById(R.id.price);
                    if (args.get("description") != null) {
                        description.setText((CharSequence) args.get("description"));
                    }
                    if (args.get("price") != null) {
                        count.setText(Long.toString((Long) args.get("price")));
                    }
                    if (args.get("count") != null) {
                        price.setText(Long.toString((Long) args.get("count")));
                    }
                }
            }
        }
        final DbHelper dbHelper = new DbHelper(getActivity());
        Button save = (Button) rootView.findViewById(R.id.buttonSave);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText description = (EditText) rootView.findViewById(R.id.description);
                EditText count = (EditText) rootView.findViewById(R.id.count);
                EditText price = (EditText) rootView.findViewById(R.id.price);
                if (description.getText().toString().trim().equals("")) {
                    description.setError("Description required!");
                    return;
                }
                if (count.getText().toString().trim().equals("")) {
                    count.setError("Count is required!");
                    return;
                }
                if (price.getText().toString().trim().equals("")) {
                    price.setError("Price is required!");
                    return;
                }
                hideKeyboard(getActivity());

                if (args == null) {
                    dbHelper.insertItem(new Item(-1, description.getText().toString().trim(), Integer.parseInt(price.getText().toString().trim()), Integer.parseInt(count.getText().toString().trim())));
                } else {
                    if (args.get("id") != null) {
                        Item toUpdate = new Item((Long) args.get("id"), description.getText().toString().trim(), Integer.parseInt(price.getText().toString().trim()), Integer.parseInt(count.getText().toString().trim()));
                        dbHelper.updateItem(toUpdate);
                    }
                }
                getActivity().getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                if (rootView.findViewById(R.id.fragment_container_edit) == null) {
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_main, new RecyclerViewFragment()).commit();
                }
            }
        });


        return rootView;
    }
}
