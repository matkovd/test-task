package com.example.daniil.testtask.fragments;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.daniil.testtask.R;
import com.example.daniil.testtask.adapter.RecyclerViewAdapter;
import com.example.daniil.testtask.db.DbHelper;
import com.example.daniil.testtask.models.Item;

import java.util.ArrayList;

/**
 * Created by Daniil on 15.12.2016.
 */

public class RecyclerViewFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.recycler_view_fragment, container, false);
        View fullView = inflater.inflate(R.layout.activity_main, container, false);
        super.onCreate(savedInstanceState);
        DbHelper dbHelper = new DbHelper(getActivity());
        ArrayList<Item> items = dbHelper.getAllItems();
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(items, getActivity(), getFragmentManager(), fullView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(itemAnimator);
        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        return rootView;
    }
}
