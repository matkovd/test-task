package com.example.daniil.testtask.adapter;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.daniil.testtask.R;
import com.example.daniil.testtask.db.DbHelper;
import com.example.daniil.testtask.fragments.EditFragment;
import com.example.daniil.testtask.models.Item;

import java.util.ArrayList;
import java.util.Objects;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private Context context;
    private FragmentManager fragmentManager;
    private ArrayList<Item> items;
    private View rootView;

    public RecyclerViewAdapter(ArrayList<Item> items, Context context, FragmentManager fragmentManager, View rootView) {
        this.items = items;
        this.context = context;
        this.fragmentManager = fragmentManager;
        this.rootView = rootView;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recyclerview_item, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Item item = items.get(i);
        viewHolder.description.setText(item.getDescription());
        viewHolder.count.setText(Long.toString(item.getCount()));
        viewHolder.price.setText(Long.toString(item.getPrice()));

    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        private final View.OnCreateContextMenuListener mOnCreateContextMenuListener = new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                MenuItem edit = menu.add("Edit");
                edit.setOnMenuItemClickListener(mEditClickListener);
                MenuItem delete = menu.add("Delete");
                delete.setOnMenuItemClickListener(mDeleteClickListener);

            }
        };
        private TextView description;
        private TextView count;
        private TextView price;
        private final MenuItem.OnMenuItemClickListener mDeleteClickListener = new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Item found = null;
                for (Item temp : items) {
                    if (temp.getPrice() == Long.parseLong(price.getText().toString())) {
                        if (Objects.equals(temp.getDescription(), description.getText().toString().trim())) {
                            if (temp.getCount() == Long.parseLong(count.getText().toString())) {
                                found = temp;
                            }
                        }
                    }
                }
                if (found == null) {
                    return false;
                }
                DbHelper dbHelper = new DbHelper(context);
                dbHelper.deleteItem((int) found.getId());
                int position = items.indexOf(found);
                items.remove(position);
                notifyItemRemoved(position);
                return true;
            }
        };
        private final MenuItem.OnMenuItemClickListener mEditClickListener = new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Item found = null;
                for (Item temp : items) {
                    if (temp.getPrice() == Long.parseLong(price.getText().toString())) {
                        if (Objects.equals(temp.getDescription(), description.getText().toString().trim())) {
                            if (temp.getCount() == Long.parseLong(count.getText().toString())) {
                                found = temp;
                            }
                        }
                    }
                }
                if (found == null) {
                    return false;
                }
                EditFragment editFragment = new EditFragment();
                Bundle data = new Bundle();
                data.putBoolean("edit", true);
                data.putString("description", found.getDescription());
                data.putLong("price", found.getPrice());
                data.putLong("count", found.getCount());
                data.putLong("id", found.getId());
                editFragment.setArguments(data);
                if (rootView.findViewById(R.id.fragment_container_edit) == null) {

                    fragmentManager.beginTransaction().replace(R.id.fragment_container_main, editFragment).commit();
                } else {
                    fragmentManager.beginTransaction().replace(R.id.fragment_container_edit, editFragment).commit();
                }
                return true;
            }
        };

        public ViewHolder(View itemView) {
            super(itemView);
            description = (TextView) itemView.findViewById(R.id.recyclerViewItemDescription);
            count = (TextView) itemView.findViewById(R.id.recyclerViewItemCount);
            price = (TextView) itemView.findViewById(R.id.recyclerViewItemPrice);
            itemView.setOnCreateContextMenuListener(mOnCreateContextMenuListener);
        }


    }


}
